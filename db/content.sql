insert into tl_liens values
(1, 'http://campus-cvtic.unilim.fr/', 'Campus Virtuel TIC 2015', 'Site internet du CvTIC.', 1),
(2, 'https://duckduckgo.com/', 'Duck Duck Go', 'Le moteur de recherche qui ne trace pas ses utilisateurs.', 1),
(3, 'https://framasoft.org/', 'Framasoft', 'Un réseau dédié à la promotion du « libre » en général et du logiciel libre en particulier.', 1),
(4, 'google.fr', 'Google', 'Un moteur de recherche qui utilise vos données', 1),
(5, 'unilim.fr', 'Unilim', 'Un site dédié à l''université de limoges', 1),
(6, 'openclassrooms.com', 'Openclassrooms', 'Un site pour apprendre à coder', 1),
(7, 'Codecademy.com', 'Codecademy', 'Apprendre les bases des langages de codage', 1),
(8, 'Dpstream.net', 'Dpstream', 'Un bon site pour le streaming', 1),
(9, 'zone-telechargement.com', 'Zone-telechargement', 'Un bon site pour télécharger des films et des séries', 1),
(10, 'Facebook.com', 'Facebook', 'Un site pour espionner les gens', 1);

/* raw password is 'adminpass' */
insert into tl_users values
(1, 'admin', 'LsJKppRTEPz4uKrkhScOE6HBSvHuaIcFbAX9FWC7h/f5HffX4TBcFt7p8M0hqvGzFXL+JV8TzEYePoimaosfMQ==', '>=28!7NLw!S37zLjs7Uu[nC', 'ROLE_ADMIN');

insert into tl_tags values
(1, 'fac'),
(2, 'vieprivee'),
(3, 'opensource'),
(4, 'apprendre'),
(5, 'recherche,'),
(6, 'moteur'),
(7, 'vieprivee,'),
(8, 'Ecorp!'),
(9, 'Universite,'),
(10, 'apprentissage'),
(11, 'apprentissage,'),
(12, 'code'),
(13, 'streaming,'),
(14, 'films,'),
(15, 'series'),
(16, 'series,'),
(17, 'telechargement'),
(18, 'Ecorp!,');

insert into tl_tags_liens values
(1, 1),
(2, 2),
(3, 2),
(3, 3),
(4, 1),
(4, 3),
(5, 5),
(6, 5),
(5, 4),
(6, 4),
(7, 4),
(8, 4),
(9, 5),
(10, 5),
(11, 6),
(12, 6),
(11, 7),
(12, 7),
(13, 8),
(14, 8),
(15, 8),
(14, 9),
(16, 9),
(17, 9),
(18, 10),
(2, 10);

